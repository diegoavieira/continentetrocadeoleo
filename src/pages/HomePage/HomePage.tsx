import React from 'react';
import { Box, Button, Divider, Fab, Theme, createStyles, withStyles } from '@material-ui/core';
import { Email, Facebook, Instagram, Phone, WhatsApp } from '@material-ui/icons';
import { RdsContainer, RdsText } from '@rdsystem/components';
import { address, email, facebook, instagram, phone, whatsapp } from 'src/constants/links';

const BoxMapStyled = withStyles(() =>
  createStyles({
    root: {
      position: 'relative',
      overflow: 'hidden',
      width: '100%',
      paddingTop: '70%',
      '& iframe': {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        width: '100%',
        height: '100%',
        border: 0
      }
    }
  })
)(Box);

const RdsTextStyled = withStyles(() =>
  createStyles({
    root: {
      overflow: 'hidden',
      textOverflow: 'ellipsis'
    }
  })
)(RdsText);

const FabStyled = withStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'fixed',
      bottom: '16px',
      right: '16px',
      fontSize: '2rem',
      color: theme.palette.common.white,
      backgroundColor: theme.palette.success.main,
      '&:hover': {
        backgroundColor: theme.palette.success.dark,
        '@media (hover: none)': {
          backgroundColor: theme.palette.success.main
        }
      }
    }
  })
)(Fab);

const HomePage = () => {
  return (
    <RdsContainer maxWidth="xs">
      <RdsText align="center" margin="8px 0 12px" color="primary">
        Entre em contato conosco!
      </RdsText>

      <Box margin="16px 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(phone, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Phone />
          <RdsText margin="0 0 0 8px">48 3247-6564</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(whatsapp, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <WhatsApp />
          <RdsText margin="0 0 0 8px">48 98401-3887</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(instagram, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Instagram />
          <RdsText margin="0 0 0 8px">@continenteoleo</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 8px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(facebook, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Facebook />
          <RdsText margin="0 0 0 8px">@continente.oleo</RdsText>
        </Button>
      </Box>

      <Box margin="0 0 16px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(email, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <Email />
          <RdsTextStyled margin="0 0 0 8px">continentetrocadeoleo@outlook.com</RdsTextStyled>
        </Button>
      </Box>

      <Divider />

      <RdsText align="center" margin="20px 0 16px" color="primary">
        Horário de funcionamento
      </RdsText>

      <RdsText align="center" margin="0 0 4px">
        Segunda a sexta das 8h às 18h30
      </RdsText>
      <RdsText align="center" margin="0 0 20px">
        Sábado das 8h ao meio-dia
      </RdsText>

      <Divider />

      <RdsText align="center" margin="20px 0 12px" color="primary">
        Venha nos conhecer!
      </RdsText>

      <Box margin="0 0 16px" display="flex" justifyContent="center">
        <Button onClick={() => window.open(address, '_blank')} style={{ textTransform: 'initial' }} fullWidth>
          <RdsText margin="0 0 0 8px">Rodovia SC-281, km 4 - Sertão do Maruim - São José/SC</RdsText>
        </Button>
      </Box>

      <BoxMapStyled marginBottom="16px">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14143.359573333222!2d-48.6763468!3d-27.598493!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95274b25212f2c51%3A0xfd3b41aa3465a8b6!2sContinente%20Troca%20de%20%C3%93leo!5e0!3m2!1spt-BR!2sbr!4v1689081222833!5m2!1spt-BR!2sbr"
          allowFullScreen
          aria-hidden="false"
          loading="lazy"
        ></iframe>
      </BoxMapStyled>
      <FabStyled onClick={() => window.open(whatsapp, '_blank')}>
        <WhatsApp fontSize="inherit" />
      </FabStyled>
    </RdsContainer>
  );
};

export default HomePage;
